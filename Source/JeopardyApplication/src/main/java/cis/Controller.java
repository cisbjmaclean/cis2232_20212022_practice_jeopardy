package cis;

import cis.bo.Game;
import cis.util.CisUtility;
import cis.util.UtilJeopardy;

/**
 * A starting project which we can use for applications that need a menu driven
 * program. Note that the name of the project should be modified to reflect the
 * specific requirements.
 *
 * @author bjmaclean
 * @since 20181115
 * @modified 2021-05-30 mdw - added option to show current scores
 */
public class Controller {

    public static final String EXIT = "X";

    private static final String MENU
            = "\n-------------------------\n"
            + "- CIS Jeopardy\n"
            + "- A-Start Game\n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option-->";

    private static Game game;

    public static void main(String[] args) {

        //Add a loop below to continuously promput the user for their choice 
        //until they choose to exit.
        String option = "";

        CisUtility.display("Today is: " + CisUtility.getCurrentDate(null), "Red");

        //Call the method which will setup the categories and clues.  
        UtilJeopardy.setup();
        game = new Game(UtilJeopardy.getClues(), UtilJeopardy.getCategoriesMap());

        //Show the categories
        do {
            option = CisUtility.getInputString(MENU, "Green");
            processMenuOption(option);
        } while (!option.equalsIgnoreCase(EXIT));

    }

    /**
     * This method will process the menu option specified in the input
     * parameter. It will call appropriate functionality based on its value.
     *
     * @param option The menu option
     * @since 20171102
     * @author cis1201b
     * @modified 2021-05-13 mdw - Added setup players and start game
     * @modified 2021-05-30 mdw - Added current points case functionality
     *
     */
    public static void processMenuOption(String option) {
        //Add a switch to process the option
        switch (option.toUpperCase()) {
            case "A":
                game.setupPlayers();
                game.play();
                break;
            case "X":
                CisUtility.display("Thanks for playing");
                break;
            default:
                CisUtility.display("Invalid entry");
        }
    }

}
