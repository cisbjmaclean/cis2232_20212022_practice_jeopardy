package cis.entity;

/**
 * Represents a category in a Jeopardy game.
 *
 * @author bjmac
 * @since 27-Apr-2021
 */
public class Category implements Comparable<Category> {

    private int id;
    private String title;

    public Category() {
    }

    public Category(int id, String title, int value) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Category other = (Category) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public void display() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return "Category " + "id=" + id + ", title=" + title;
    }

    @Override
    public int compareTo(Category that) {
        return this.id - that.getId();
    }

}
