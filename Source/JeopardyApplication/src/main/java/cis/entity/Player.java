package cis.entity;

import cis.util.CisUtility;

/**
 * Class represents the user-controlled Jeopardy contestant
 * 
 * @author mdw
 * @since 2021-05-10
 */
public class Player{

    protected String name;
    protected String job;
    protected String hometown;
    protected boolean returningChampion;
    protected String interestingFact;
    protected int score = 0;

    public void getInformation() {
        try {
            name = CisUtility.getInputString("Name:");
        } catch (Exception e) {
            System.out.println("Invalid entry. Please try again.");
        }
        try {
            job = CisUtility.getInputString("What do you do for a living?");
        } catch (Exception e) {
            System.out.println("Invalid entry. Please try again.");
        }
        try {
            hometown = CisUtility.getInputString("Where are you from?");
        } catch (Exception e) {
            System.out.println("Invalid entry. Please try again.");
        }
        try {
            interestingFact = CisUtility.getInputString("What is something interesting about you?");
        } catch (Exception e) {
            System.out.println("Invalid entry. Please try again.");
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public boolean isReturningChampion() {
        return returningChampion;
    }

    public void setReturningChampion(boolean returningChampion) {
        this.returningChampion = returningChampion;
    }

    public String getInterestingFact() {
        return interestingFact;
    }

    public void setInterestingFact(String interestingFact) {
        this.interestingFact = interestingFact;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void display() {
        CisUtility.display(this.toString());
    }

    @Override
    public String toString() {
        return "Name: " + name + " Job: " + job + " Hometown: " + hometown + " Returning Champion: " + returningChampion + " Interesting Fact: " + interestingFact + " score:" + score;
    }


    
}