package cis.entity;

import cis.util.CisUtility;

/**
 * Represents a clue (aka question) for Jeopardy game
 *
 * @author bjmac
 * @since 27-Apr-2021
 */
public class Clue {

    private int id;
    private String answer;
    private String question;
    private int category_id;  //Note naming to match the json which is used to obtain the questions.
    private Category category;
    private int value;

    public Clue() {
        //default
    }

    public Clue(int category, int value) {
        this.category_id = category;
        this.value = value;
    }

    public Clue(int id, String answer, String question, int category_id, Category category, int value) {
        this.id = id;
        this.answer = answer;
        this.question = question;
        this.category_id = category_id;
        this.category = category;
        this.value = value;
    }

    public boolean processQuestion(String name){
        String answer = getUserAnswer(name);
        return processRightOrWrong(answer);
    }
    
    /**
     * Get the answer from the user. This may be overwrite'd based on the type
     * of clue (ie. short answer, multiple choice....).
     *
     * @since 20210607
     * @author BJM
     */
    public String getUserAnswer(String playerName) {
        String answer = CisUtility.getInputString(playerName + "... please provide an answer:");
        return answer;
    }

    /**
     * Process right or wrong. Will ask for human validation in case it's not an
     * exact match.
     *
     * @since 20210602
     * @author BJM
     */
    public boolean processRightOrWrong(String answer) {
        CisUtility.display("The correct answer is --> " + this.getAnswer());
        boolean correct;
        if (this.getAnswer().equalsIgnoreCase(answer)) {
            correct = true;
        } else {
            //Not an exact match, ask the judges.
            correct = CisUtility.getInputBoolean("Judges... was the answer acceptable?");
        }
        return correct;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void displayAnswer() {
        System.out.println("Clue# " + id + " answer=" + answer + " value=" + value);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.category_id;
        hash = 29 * hash + this.value;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Clue other = (Clue) obj;
        if (this.category_id != other.category_id) {
            return false;
        }
        if (this.value != other.value) {
            return false;
        }
        return true;
    }

    public void display() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return "Clue# " + id + " answer=" + answer + ", question=" + question + ", category_id=" + category_id + ", value=" + value;
    }

}
